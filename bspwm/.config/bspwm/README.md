# Bspwm config
This is my configuration of the bspwm. It relies on sxhkd as normal bspwm does. Also, I use [my polybar config](https://gitlab.com/nnonfree/dotfiles/-/tree/main/polybar/.config/polybar) and tdrop with my bspwm config.

## Here are some basic keybindinkgs:

| Keybinding | Action |
|------------|--------|
| `Super + Return` or `Super + Shift + Return` | brings up a terminal(alacritty) |
| `Super + p` | brings up an application launcher(dmenu) |
| `Super + w` | brings up a web browser(firefox) |
| `Super + Shift + c` or `Super + q` | closes the current client |
| `Super + x` | brings up the dmenu logout script |

![screenshot](https://gitlab.com/nnonfree/dotfiles/-/raw/main/bspwm/.config/bspwm/screenshot.png)
