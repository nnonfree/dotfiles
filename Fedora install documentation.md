# Fedora post-installation documentation
I must admit, that I used "Fedora Everything" install image. I chose Workstation flavour as the base with scientific Python, C development stuff, LibreOffice, window managers and text editors(emacs and vi) additionally ticked on.
## First of all, I've added rpmfusion repo this way:
``` bash
# Free software repo
sudo dnf install \
  https://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm
# Non-free software
sudo dnf install \
  https://download1.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm
```
and ran into the **pid 3811 dnf** issue while installing my needed packages. I fixed it by **removing the dnf cache lockfile**:
``` bash
sudo rm -rf /var/cache/dnf/*pid
```
## After that, I installed several applications with dnf, pip and cargo:
``` bash
sudo dnf install stow zsh fish neovim emacs exa ripgrep curl lxpolkit bspwm sxhkd nm-connection-editor dunst qrencode i3lock rofi polybar nnn bpytop htop neofetch picom nitrogen syncthing git git-annex apostrophe opendoas clang golang sbcl python3 python3-pip lua ruby irb openjdk-asmtools java-17-openjdk-devel rust cargo alsa-utils playerctl brightnessctl cmus fd-find kakoune cava sxiv thefuck tar gtk-murrine-engine qutebrowser ulauncher
sudo pip install qtile
cargo install rm-improved # launches with "rip" command
```
Install unimatrix:
``` bash
sudo curl -L https://raw.githubusercontent.com/will8211/unimatrix/master/unimatrix.py -o /usr/local/bin/unimatrix
sudo chmod a+rx /usr/local/bin/unimatrix
```
For coc.nvim:
``` bash
curl -sL install-node.vercel.app/lts | sudo bash
```
For gzdoom:
``` bash
sudo dnf copr enable nalika/gzdoom
sudo dnf install gzdoom
```
For lf file manager:
``` bash
wget https://github.com/gokcehan/lf/releases/download/r27/lf-linux-amd64.tar.gz \
-O lf-linux-amd64.tar.gz
tar xvf lf-linux-amd64.tar.gz
sudo mv lf /bin/
rm -rf lf-linux-amd64.tar.gz
```
### Then installed the quicklisp plugin manager:
``` bash
curl -o /tmp/ql.lisp http://beta.quicklisp.org/quicklisp.lisp
sbcl --no-sysinit --no-userinit --load /tmp/ql.lisp \
      --eval '(quicklisp-quickstart:install :path "~/.quicklisp")' \
      --eval '(ql:add-to-init-file)' \
      --quit
```
### And I needed to install iwlib for qtile:
First, I've installed trinity repo:
`sudo dnf install http://mirror.ppa.trinitydesktop.org/trinity/rpm/f36/trinity-r14/RPMS/noarch/trinity-repo-14.0.12-1.fc36.noarch.rpm`
Then installed the wireless tools package:
`sudo dnf install wireless-tools-devel`
And finally, install the python's iwlib itself:
`sudo pip install iwlib`
### Also, I downloaded 4 unicode fonts
* [CascadiaCode Nerd Font](https://github.com/ryanoasis/nerd-fonts/releases/download/v2.2.2/CascadiaCode.zip)
* [FiraCode Nerd Font](https://github.com/ryanoasis/nerd-fonts/releases/download/v2.2.2/FiraCode.zip)
* [JetBrainsMono Nerd Font](https://github.com/ryanoasis/nerd-fonts/releases/download/v2.1.0/JetBrainsMono.zip)
* [DejaVuSansMono wifi ramp(for polybar)](https://github.com/isaif/polybar-wifi-ramp-icons/raw/main/DejaVuSansMono-wifi-ramp.ttf)

and unpacked them to **~/.fonts** folder.

### Then, I added the flathub repo for flatpak
``` bash
flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
```
### Also, I wanted to get virtual cam for obs work:
> This is NOT going to work
1. Install the obs studio and dependencies:
``` bash
sudo dnf installobs-studio obs-studio-devel obs-studio-libs qt5-qtbase-devel
dnf copr enable sentry/v4l2loopback 
sudo dnf install v4l2loopback
modprobe v4l2loopback
```
2. Reboot:
``` bash
systemctl reboot
```

## Afterall, I needed HarfBuzz for st:
1. Install required packages:
``` bash
sudo dnf install gcc gcc-c++ freetype-devel glib2-devel cairo-devel meson pkgconfig gtk-doc
```
2. Get sources:
``` bash
git clone https://github.com/harfbuzz/harfbuzz
```
3. And compile:
``` bash
meson build
meson compile -C build
```

## Then, I've copied and set my dotfiles up:
> Note: Unfortunately, I still haven't uploaded all of my dotfiles on a gitlab :|.

To get all of my dotfiles, clone [this repo](https://gitlab.com/nnonfree/dotfiles/).

To manage my dotfiles, I use GNU/Stow.
### Emacs
I use the Doom Emacs configuration framework, so it needs to be installed first:
``` bash
git clone --depth 1 https://github.com/doomemacs/doomemacs ~/.emacs.d
~/.emacs.d/bin/doom install
```
And then just add the symlink to it with `stow doom-emacs`.
### Neovim
Since I use *vim-plug* as my plugin manager, it needs to be installed:
``` bash
sh -c 'curl -fLo "${XDG_DATA_HOME:-$HOME/.local/share}"/nvim/site/autoload/plug.vim --create-dirs \
       https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
```
> Neovim needs a special variation of vim-plug, because it is not 100% compatible with vim

I use coc.nvim, so let's install some extensions:
``` vim
:CocInstall coc-clangd coc-pyright coc-rls coc-toml coc-yaml coc-lua coc-json coc-go
```

Then, I just opened up neovim and having entered the command mode, typed **```PlugInstall```**.
### Helix text editor
> To install helix I needed to enable it's copr repo first:
``` bash
sudo dnf copr enable varlad/helix
sudo dnf install helix
```
Also, I wanted autocompletion to work, so I installed some language servers:
* *Bash language server* provided by **nodejs-bash-language-server**.
* *Clangd* provided by **clang-tools-extra**.
* *Cmake language server* installed with **python pip** provided by **cmake-language-server**.
* *Gopls* provided by **golang-x-tools-gopls**.
* *Lua language server* installed **manually**.
* *Pylsp* provided by **pylsp** package.
* *Rust analyzer* provided by **rust-analyzer** in copr.
* *Taplo* installed with **cargo** provided by **taplo-cli**.
``` bash
sudo dnf copr enable robot/rust-analyzer # add rust-analyzer copr repo
sudo dnf install nodejs-bash-language-server clang-tools-extra golang-x-tools-gopls rust-analyzer
sudo pip install cmake-language-server
cargo install taplo-cli --locked
```
> For lua lsp:

First, get the linux **x64** pre-built binary archive [here](https://github.com/sumneko/lua-language-server/releases) and extract it:
``` bash
mkdir ~/.lua-lsp
tar -xzf lua-language-server-*-linux-x64.tar.gz -C ~/.lua-lsp
```
Then, as it is extracted, I linked the "lua-language-server" file to "~/.local/bin/" folder so that lua language server can work:
``` bash
mkdir ~/.local/bin
ln -s ~/.lua-lsp/bin/lua-language-server ~/.local/bin
```
### Fish shell
First thing first, Fedora does not have the *"chsh"* command avaliable, so I needed to install the package which provides it:
``` bash
dnf provides chsh # Do dicover which package provides this command
sudo dnf install util-linux-user
```
Then, I installed oh my fish plugin manager:
``` bash
curl https://raw.githubusercontent.com/oh-my-fish/oh-my-fish/master/bin/install | fish
```
and turned on vi key bindings:
```
fish_vi_key_bindings
```
The next thing was to install my prefered theme:
```
omf install bobthefish
```
### Opendoas
Since I prefer doas over sudo, I needed to write opendoas config. Just put *this* to ***/etc/doas.conf***:
``` configuration
permit persist :wheel
```
### Tdrop scratchpad
It required some basic x11 tools like:
* xrpop
* xwininfo
* xdotool
``` bash
sudo dnf install xprop xwininfo xdotool
```
After thar, I made new folder in the root of my home directory named **"Repos"** and **cloned the github repository there**:
``` bash
cd # To go back to my home directory
mkdir Repos && cd Repos
git clone https://github.com/noctuid/tdrop
```
And finally, instalation:
``` bash
sudo make install
```
### Mouse tweaks
I wanted to enable "Tap to click" feature on every X11 window manager(bspwm, dwm, etc.), so I made a new file at *`/etc/X11/xorg.conf.d/touchpad-tap.conf`* with the following in it:
``` configuration
Section "InputClass"
        Identifier "libinput touchpad catchall"
        MatchIsTouchpad "on"
        MatchDevicePath "/dev/input/event*"
        Driver "libinput"
        Option "Tapping" "on"
EndSection
```
### Qtile
Basically there's nothing to do here. Just copy the **qtile.desktop** file to ***/usr/share/xsession*** directory:
``` bash
sudo cp -r ~/.config/qtile/qtile.desktop /usr/share/xsessions
```
### Suckless software
> First of all, I needed to install some essential X libraries reqired by my builds of dwm, dmenu, st, slsatus and slock:
* **Xlib.h** provided by ***libX11-devel*** package
* **Xinerama.h** provided by ***libXinerama-devel*** package
* **Xft.h** provided by ***libXft-devel*** package

``` bash
sudo dnf install libX11-devel libXinerama-devel libXft-devel libXrandr-devel imlib2-devel
```
#### After that, the process was pretty strait forward.
Simply just run **```sudo make clean install```** in every child directory of ".suckless".
### Theming
For theming, I needed to install some packages:
* **Lxappearance** for *Gtk*
* **qt5ct** for *Qt*
* **Grub customizer** for grub
``` bash
sudo dnf install lxappearance qt5ct grub-customizer
```
Also, I use [jaxwilko's gtk theme framework](https://github.com/jaxwilko/gtk-theme-framework).

---------
**![Work in progress](https://workinprogress.no/dynamic/upload/bilder/Work-In-Progress.png)**
