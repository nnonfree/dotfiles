#  _________  _   _
# |__  / ___|| | | |_ __ ___
#   / /\___ \| |_| | '__/ __|
#  / /_ ___) |  _  | | | (__
# /____|____/|_| |_|_|  \___|

# Fetch tool
function greetings()
{
	# Greeting message
	echo -e "\033[94m`uname --kernel-name --kernel-release --hardware-platform`\n\033[0m"
	echo -e	"\033[94mLogged in on \033[33m`date "+%d/%m/%y"`\033[0m \033[94mat\033[33m`date "+%l:%M"`\033[0m"
    echo -e "\033[35m`uptime -p`"; echo "" 

	echo -e "\033[0mWelcome on \033[34m$HOST\033[0m.\n"
}
greetings



# Aliases
alias "btop"="bpytop" 
alias "clr"="clear" 
alias "grep"="rg" 
alias "kakrc"="kak ~/.config/kak/kakrc" 
alias "kaktutor"="kak ~/.config/kak/TRAMPOLINE" 
alias "l"="exa --icons -lbFh"
alias "la"="exa -lbhHigUmuSa --icons --time-style=long-iso --git"
alias "ll"="exa --icons -albFh"
alias "ls"="exa --icons"
alias "nnn"="nnn -C"
alias "nv"="nvim"
alias "r"="ranger"
alias "rudo"="/bin/sudo"
alias "sl"="exa --icons"
alias "sudo"="doas"
alias "tree"="exa --icons --tree --level=2"
alias "v"="nvim"
alias "vm"="mv"
alias "vn"="nvim"
alias "x"="exit"
alias "x;"="hx"



# Export terminal variables
path+=('~/.local/bin')
export PATH
export EDITOR="hx"


# Vi keybindnings
source $HOME/.zsh/plugins/zsh-vi-mode/zsh-vi-mode.plugin.zsh
# bindkey -v

# Calculator
source $HOME/.zsh/plugins/calc/calc.plugin.zsh

# Tab completion
# fpath=($HOME/.zsh/plugins/zsh-completions/src $fpath)
# autoload -U compinit && compinit
# source $HOME/.zsh/plugins/zsh-completions/zsh-completions.plugin.zsh

# Syntax highlighting
source $HOME/.zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh

# Autocompletion
source $HOME/.zsh/plugins/zsh-autocomplete/zsh-autocomplete.plugin.zsh

# Thefuck
# eval $(thefuck --alias)



# Powerlevel10k
source ~/.zsh/powerlevel10k/powerlevel10k.zsh-theme

# Shorten the path
POWERLEVEL9K_SHORTEN_DIR_LENGTH=2

# Enable Powerlevel10k instant prompt. Should stay close to the top of ~/.zshrc.
# Initialization code that may require console input (password prompts, [y/n]
# confirmations, etc.) must go above this block; everything else may go below.
if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
  source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
fi

# To customize prompt, run `p10k configure` or edit ~/.p10k.zsh.
[[ ! -f ~/.p10k.zsh ]] || source ~/.p10k.zsh
