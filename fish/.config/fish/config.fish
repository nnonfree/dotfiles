# Autostart scripts
# colorscript -r
# treefetch

set -g theme_display_date no
set -g theme_color_scheme nord # if theme = bobthefish
# fish_config theme save "Catppuccin Mocha"

if status is-interactive
    # Commands to run in interactive sessions can go here
    set fish_greeting
    fish_vi_key_bindings

    export EDITOR="nvim"
    export BROWSER="firefox"
    export TERMINAL="alacritty"
    # export BAT_THEME="Catppuccin-mocha"
    export PF_INFO="ascii os host kernel uptime pkgs editor memory"
end

# Add directories to $PATH
fish_add_path /home/vlad/go/bin
fish_add_path /home/vlad/.cargo/bin
fish_add_path /home/vlad/.local/bin
fish_add_path /home/vlad/.spicetify
fish_add_path /home/vlad/.emacs.d/bin
fish_add_path /home/vlad/.lua-lsp/bin/

# Source commands:
# thefuck --alias | source
# starship init fish | source
