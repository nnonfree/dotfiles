;==========================================================
;
;
;   ██████╗  ██████╗ ██╗  ██╗   ██╗██████╗  █████╗ ██████╗
;   ██╔══██╗██╔═══██╗██║  ╚██╗ ██╔╝██╔══██╗██╔══██╗██╔══██╗
;   ██████╔╝██║   ██║██║   ╚████╔╝ ██████╔╝███████║██████╔╝
;   ██╔═══╝ ██║   ██║██║    ╚██╔╝  ██╔══██╗██╔══██║██╔══██╗
;   ██║     ╚██████╔╝███████╗██║   ██████╔╝██║  ██║██║  ██║
;   ╚═╝      ╚═════╝ ╚══════╝╚═╝   ╚═════╝ ╚═╝  ╚═╝╚═╝  ╚═╝
;
;
;   To learn more about how to configure Polybar
;   go to https://github.com/polybar/polybar
;
;   The README contains a lot of information
;
;==========================================================

[colors]
background = #2E3440
background-alt = #3B4252
foreground = #ECEFF4
primary = #EBCB8B
secondary = #81A1C1
title = #B48EAD
alert = #BF616A
finished = #8FBCBB
disabled = #4C566A

[bar/example]
width = 100%
height = 16pt
radius = 0

; dpi = 96

background = ${colors.background}
foreground = ${colors.foreground}

line-size = 2pt

border-size = 0pt
border-color = #00000000

padding-left = 0
padding-right = 1

module-margin = 1

separator = |
separator-foreground = ${colors.disabled}

font-0 = JetBrainsMono Nerd Font:size=10.5;2
font-1 = DejaVu Sans Mono wifi ramp:size=10.5;1

# modules-left = xworkspaces i3 polywins
modules-left = xworkspaces polywins
modules-right = xkeyboard pulseaudio battery wlan date

cursor-click = pointer
cursor-scroll = ns-resize

enable-ipc = true

tray-position = right

; wm-restack = generic
wm-restack = bspwm
; wm-restack = i3

override-redirect = false

[module/xworkspaces]
type = internal/xworkspaces

label-active = %name%
label-active-background = ${colors.background-alt}
label-active-foreground= ${colors.primary}
label-active-underline= ${colors.primary}
label-active-padding = 1

label-occupied = %name%
label-occupied-padding = 1

label-urgent = %name%
label-urgent-background = ${colors.alert}
label-urgent-padding = 1

; label-empty = %name%
label-empty =
label-empty-foreground = ${colors.disabled}
label-empty-padding = 1

[module/polywins]
type = custom/script
exec = ~/.config/polybar/scripts/polywins.sh 2>/dev/null
format = <label>
label = %output%
label-padding = 1
tail = true

[module/xwindow]
type = internal/xwindow
label-foreground = ${colors.title}
label = %title:0:60:...%

[module/i3]
type = internal/i3
format = <label-mode>

label-mode-foreground = ${colors.secondary}
label-mode-padding = 1

[module/pulseaudio]
type = internal/pulseaudio

format-volume = <ramp-volume> <label-volume>
format-muted = <label-muted>

label-muted = ﱝ
label-volume = %percentage%%

enable-scroll = false

; Volume emojis
ramp-volume-0 = 奄
ramp-volume-1 = 奄
ramp-volume-2 = 奔
ramp-volume-3 = 墳

; label-muted = muted
label-muted-foreground = ${colors.disabled}

[module/xkeyboard]
type = internal/xkeyboard
blacklist-0 = num lock

format-prefix = "  "
; format-prefix-foreground = ${colors.primary}

label-layout = %layout%
; label-layout-foreground = ${colors.primary}

label-indicator-padding = 2
label-indicator-margin = 1
label-indicator-foreground = ${colors.background}
label-indicator-background = ${colors.secondary}

[module/memory]
type = internal/memory
interval = 2
format-prefix = "RAM "
format-prefix-foreground = ${colors.primary}
label = %percentage_used:2%%

[module/cpu]
type = internal/cpu
interval = 2
format-prefix = "CPU "
format-prefix-foreground = ${colors.primary}
label = %percentage:2%%

[module/battery]
type = internal/battery
; This is useful in case the battery never reports 100% charge
full-at = 95
; format-low once this charge percentage is reached
low-at = 15
; Use the following command to list batteries and adapters:
; $ ls -1 /sys/class/power_supply/
battery = BAT0
adapter = ADP1
; If an inotify event haven't been reported in this many
; seconds, manually poll for new values.
; Needed as a fallback for systems that don't report events
; on sysfs/procfs.
poll-interval = 5

; see "man date" for details on how to format the time string
; NOTE: if you want to use syntax tags here you need to use %%{...}
; Default: %H:%M:%S
time-format = %H:%M

; Available tags:
;   <label-charging> (default)
;   <bar-capacity>
;   <ramp-capacity>
;   <animation-charging>
format-charging = <animation-charging>  <label-charging>

; Available tags:
;   <label-discharging> (default)
;   <bar-capacity>
;   <ramp-capacity>
;   <animation-discharging>
format-discharging = <ramp-capacity>  <label-discharging>

; Available tags:
;   <label-full> (default)
;   <bar-capacity>
;   <ramp-capacity>
;format-full = <ramp-capacity> <label-full>

; Format used when battery level drops to low-at
; If not defined, format-discharging is used instead.
; Available tags:
;   <label-low>
;   <animation-low>
;   <bar-capacity>
;   <ramp-capacity>
; New in version 3.6.0
;format-low = <label-low> <animation-low>

; Available tokens:
;   %percentage% (default) - is set to 100 if full-at is reached
;   %percentage_raw%
;   %time%
;   %consumption% (shows current charge rate in watts)
label-charging = Charging %percentage%%

; Available tokens:
;   %percentage% (default) - is set to 100 if full-at is reached
;   %percentage_raw%
;   %time%
;   %consumption% (shows current discharge rate in watts)
label-discharging = Discharging %percentage%%

; Available tokens:
;   %percentage% (default) - is set to 100 if full-at is reached
;   %percentage_raw%
label-full = %{F#8FBCBB}  Fully charged

; Available tokens:
;   %percentage% (default) - is set to 100 if full-at is reached
;   %percentage_raw%
;   %time%
;   %consumption% (shows current discharge rate in watts)
; New in version 3.6.0
label-low = %{F#BF616A}  BATTERY LOW

; Only applies if <ramp-capacity> is used
ramp-capacity-0 = 
ramp-capacity-1 = 
ramp-capacity-2 = 
ramp-capacity-3 = 
ramp-capacity-4 = 

; Only applies if <bar-capacity> is used
bar-capacity-width = 10

; Only applies if <animation-charging> is used
animation-charging-0 = 
animation-charging-1 = 
animation-charging-2 = 
animation-charging-3 = 
animation-charging-4 = 
; Framerate in milliseconds
animation-charging-framerate = 750

; Only applies if <animation-discharging> is used
animation-discharging-0 = 
animation-discharging-1 = 
animation-discharging-2 = 
animation-discharging-3 = 
animation-discharging-4 = 
; Framerate in milliseconds
animation-discharging-framerate = 500

; Only applies if <animation-low> is used
; New in version 3.6.0
animation-low-0 = !
animation-low-1 =
animation-low-framerate = 200

[network-base]
type = internal/network
interval = 5
format-connected = <label-connected>
format-disconnected = <label-disconnected>
label-disconnected = %{F#EBCB8B}%ifname%%{F#4C566A} disconnected

[module/wlan]
inherit = network-base
interface-type = wireless
; label-connected = %{F#EBCB8B}%ifname%%{F-} %essid% %local_ip%

interval = 1.0
accumulate-stats = true
unknown-as-up = true

label-connected = %essid% %{F#EBCB8B}%local_ip%%{F-}
label-disconnected = 

format-connected = <ramp-signal> <label-connected>
format-disconnected = <label-disconnected>

ramp-signal-0 = 
ramp-signal-1 = 
ramp-signal-2 = 
ramp-signal-3 = 
ramp-signal-4 = 
ramp-signal-5 = 

[module/eth]
inherit = network-base
interface-type = wired
label-connected = %{F#EBCB8B}%ifname%%{F-} %local_ip%

[module/date]
type = internal/date
interval = 1

date = %H:%M
date-alt = %Y-%m-%d %H:%M:%S

format-prefix = "  "
; format-prefix-foreground = ${colors.primary}

label = %date%
; label-foreground = ${colors.primary}

[settings]
screenchange-reload = true
pseudo-transparency = true

; vim:ft=dosini
