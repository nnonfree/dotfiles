#        _   _ _                         __ _       
#   __ _| |_(_) | ___    ___ ___  _ __  / _(_) __ _ 
#  / _` | __| | |/ _ \  / __/ _ \| '_ \| |_| |/ _` |
# | (_| | |_| | |  __/ | (_| (_) | | | |  _| | (_| |
#  \__, |\__|_|_|\___|  \___\___/|_| |_|_| |_|\__, |
#     |_|                                     |___/ 
#                                   by pantheonfordevil

from typing import List

from libqtile import bar, layout, widget, hook, qtile
from libqtile.config import Click, Drag, Group, ScratchPad, DropDown, Key, Match, Screen
from libqtile.lazy import lazy
# from libqtile.utils import guess_terminal

import subprocess

@hook.subscribe.startup
def autostart():
    subprocess.Popen(['/home/vlad/.config/qtile/autostart.sh'])
    top.show(False)

mod      = "mod4"
# terminal = "alacritty"
terminal = "kitty"
# terminal = guess_terminal()
secterm  = "st"
browser  = "firefox"
bgrounds = "nitrogen"
editor   = "emacs"
launcher = "dmenu_run"
seclaunch= "rofi -display-combi \"Run  \" -show combi -show-icons"
lockscr  = "slock"
dmenunm  = "/home/vlad/.suckless/dmenu-flexipatch/scripts/networkmanager_dmenu"

if launcher == "dmenu_run":
    logout = "/home/vlad/.suckless/dmenu-flexipatch/scripts/logout.sh"
else:
    logout = "rofi -show power-menu -config ~/.config/rofi/power.rasi -modi power-menu:~/.config/rofi/rofi-power-menu"

prtsc    = "scrot '%Y-%m-%d-%T\_scrot.png' -e 'mv $f \~/Картинки/Screenshots'"
prtsel   = "scrot '%Y-%m-%d-%T\_scrot_cut.png' -e 'mv $f \~/Картинки/Screenshots' -s"

micmt    = "amixer set Capture toggle"
volmt    = "amixer sset Master toggle"
volup    = "amixer sset Master 5%+"
voldn    = "amixer sset Master 5%-"
mplay    = "playerctl play-pause"
mnext    = "playerctl next"
mprev    = "playerctl previous"
mstop    = "playerctl stop"
brightup = "brightnessctl set 5%+"
brightdn = "brightnessctl set 5%-"


keys = [
    ### Basic client operations
    Key([mod], "h", lazy.layout.left()),
    Key([mod], "j", lazy.layout.down()),
    Key([mod], "k", lazy.layout.up()),
    Key([mod], "l", lazy.layout.right()),
    Key([mod, "shift"], "h", lazy.layout.shuffle_left()),
    Key([mod, "shift"], "j", lazy.layout.shuffle_down(), lazy.layout.section_down()),
    Key([mod, "shift"], "k", lazy.layout.shuffle_up(), lazy.layout.section_up()),
    Key([mod, "shift"], "l", lazy.layout.shuffle_right()),
    Key(["mod1"], "j", lazy.layout.grow_down()),
    Key(["mod1"], "k", lazy.layout.grow_up()),
    Key(["mod1"], "h", lazy.layout.grow_left()),
    Key(["mod1"], "l", lazy.layout.grow_right()),
    Key([mod, "control"], "h", lazy.layout.swap_column_left()),
    Key([mod, "control"], "j", lazy.layout.move_down()),
    Key([mod, "control"], "k", lazy.layout.move_up()),
    Key([mod, "control"], "l", lazy.layout.swap_column_right()),
    Key([mod], "Return", lazy.layout.toggle_split()),
    Key([mod], "n", lazy.layout.normalize()),
    
    # Key([mod], "h", lazy.layout.shrink()),
    # Key([mod], "j", lazy.layout.down()),
    # Key([mod], "k", lazy.layout.up()),
    # Key([mod], "l", lazy.layout.grow()),
    # Key([mod, "shift"], "h", lazy.layout.swap_left(), lazy.layout.move_left()),
    # Key([mod, "shift"], "j", lazy.layout.shuffle_down(), lazy.layout.section_down()),
    # Key([mod, "shift"], "k", lazy.layout.shuffle_up(), lazy.layout.section_up()),
    # Key([mod, "shift"], "l", lazy.layout.swap_right(), lazy.layout.move_right()),
    # Key([mod, "control"], "j", lazy.layout.move_down()),
    # Key([mod, "control"], "k", lazy.layout.move_up()),
    
    Key([mod], "f", lazy.window.toggle_fullscreen()),
    Key([mod], "m", lazy.window.toggle_maximize()),
    Key([mod, "shift"], "m", lazy.spawn("Qminimize -m"),
        desc = "Minimize window"),
    Key([mod, "control"], "m", lazy.spawn("Qminimize -u"),
        desc = "Unminimize window"),
    Key([mod, "shift"], "f", lazy.window.toggle_floating()),

    ### Launchers
    Key([mod, "shift"], "Return", lazy.spawn(terminal),
        desc = "Launch terminal"),
    Key([mod, "control"], "Return", lazy.spawn(secterm),
        desc = "Launch second terminal"),
    Key([mod, "shift"], "e", lazy.spawn(editor),
        desc = "Launch text editor"),
    Key([mod], "w", lazy.spawn(browser),
        desc = "Launch main browser"),
    Key([mod, "shift"], "n", lazy.spawn(dmenunm),
        desc = "Launch dmenu network_manager"),
    Key([mod, "control"], "n", lazy.spawn(bgrounds),
        desc = "Launch nitrogen"),
    Key([mod], "p", lazy.spawn(launcher),
        desc = "Launcher"),
    Key([mod, "mod1"], "l", lazy.spawn(lockscr),
        desc = "Lockscreen"),
    Key([mod, "shift"], "p", lazy.spawn(seclaunch),
        desc = "Launch rofi"),
    Key([], "Print", lazy.spawn(prtsc)),
    Key(["shift"], "Print", lazy.spawn(prtsel)),
    Key([mod], "x", lazy.spawn(logout),
        desc = "Logout"),
    Key([mod], "r", lazy.spawncmd(),
        desc = "Spawn a command using a prompt widget"),

    ### Layouts
    Key([mod, "shift"], "Tab", lazy.next_layout(),
        desc = "Toggle between layouts"),
    Key([mod, "shift"], "c", lazy.window.kill(),
        desc = "Kill focused window"),
    Key([mod], "b", lazy.hide_show_bar("top"),
        desc = 'toggle top bar'),
    Key([mod], "n", lazy.layout.normalize()),
    Key([mod], "o", lazy.layout.maximize()),
    # Key([mod], "Return", lazy.layout.flip()),

    ### Hardware controlls
    Key([], "XF86AudioMute", lazy.spawn(volmt)),
    Key([], "XF86AudioMicMute", lazy.spawn(micmt)),
    Key([], "XF86AudioRaiseVolume", lazy.spawn(volup)),
    Key([], "XF86AudioLowerVolume", lazy.spawn(voldn)),
    Key([], "XF86AudioPlay", lazy.spawn(mplay)),
    Key([], "XF86AudioNext", lazy.spawn(mnext)),
    Key([], "XF86AudioPrev", lazy.spawn(mprev)),
    Key([], "XF86AudioStop", lazy.spawn(mstop)),
    Key([], "XF86MonBrightnessUp", lazy.spawn(brightup)),
    Key([], "XF86MonBrightnessDown", lazy.spawn(brightdn)),

    ### Qtile system
    Key([mod], "space", lazy.widget["keyboardlayout"].next_keyboard(), 
        desc="Next keyboard layout."),
    Key([mod, "shift"], "r", lazy.restart(), desc="Restart Qtile"),
    Key([mod, "shift"], "q", lazy.shutdown(), desc="Shutdown Qtile"),
]

# groups = [Group(i) for i in "123456789"]

groups = [
    Group('1', label = "α:  ", matches=[Match(wm_class = 'emacs'), Match(wm_class = 'neovide')]),
    Group('2', label = "β:  ", matches=[Match(wm_class = 'firefox'), Match(wm_class = "brave-browser"), Match(wm_class = "qutebrowser")]),
    Group('3', label = "γ:  ", matches=[Match(wm_class = "org.gnome.Nautilus"), Match(wm_class = "lxappearance"), Match(wm_class = "thunar"), Match(wm_class = "pcmanfm")]),
    Group('4', label = "δ:  ", matches=[Match(wm_class = "xonotic-sdl"), Match(wm_class = "gzdoom"), Match(wm_class = "org-tlauncher-tlauncher-rmo-TLauncher")]),
    Group('5', label = "ε:  ", matches=[Match(wm_class = 'TelegramDesktop'), Match(wm_class = 'discord'), Match(wm_class = "ViberPC"), Match(wm_class = "zoom ")], layout = "treetab"),
    Group('6', label = "ζ:  ", matches=[Match(wm_class = 'libreoffice'), Match(wm_class = 'evince'), Match(wm_class = 'okular'), Match(wm_class = 'DesktopEditors')]),
    Group('7', label = "η:  ", matches=[Match(wm_class = 'nitrogen'), Match(wm_class = 'feh'), Match(wm_class = "gimp"), Match(wm_class = 'sxiv')]),
    Group('8', label = "θ:  ", matches=[Match(wm_class = 'rhythmbox'), Match(wm_class = 'youtube-music-desktop-app'), Match(wm_class = "Spotify")]),
    Group('9', label = "ι:  ", matches=[Match(wm_class = 'mpv'), Match(wm_class = "simplescreenrecorder")]),
    Group('0', label = "κ: ",  matches=[Match(wm_class = 'Gnome-boxes')]),
]


def latest_group(qtile):
    qtile.current_screen.set_group(qtile.current_screen.previous_group)


for i in groups:
    keys.extend([
        # mod4 + letter of group = switch to group
        Key([mod], i.name, lazy.group[i.name].toscreen(),
            desc = "Switch to group {}".format(i.name)),

        # # mod4 + shift + letter of group = switch to & move focused window to group
        # Key([mod, "shift"], i.name, lazy.window.togroup(i.name, switch_group=True),
        #     desc = "Switch to & move focused window to group {}".format(i.name)),

        # Or, use below if you prefer not to switch to that group.
        # # mod4 + shift + letter of group = move focused window to group
        Key([mod, "shift"], i.name, lazy.window.togroup(i.name),
            desc = "move focused window to group {}".format(i.name)),
        
        Key([mod], "Tab", lazy.function(latest_group))
    ])

# Append scratchpad with dropdowns to groups
groups.append(ScratchPad('scratchpad', [
    DropDown('term', terminal, width=0.7, height=0.8, x=0.15, y=0.1, opacity=1),
]))
# extend keys list with keybinding for scratchpad
keys.extend([
    Key([mod], "s", lazy.group['scratchpad'].dropdown_toggle('term')),
])


### Colorschemes ###
## #*.xx - transparent
adwaita = [
    ["#000000"], # 0. panel background
    ["#1E1E1E"], # 1. darker / lighter background
    ["#f6f5f4"], # 2. active / default widget foreground
    ["#5e5c64"], # 3. inactive window border / foreground
    ["#f9f06b"], # 4. highlight color
    ["#99c1f1"], # 5. highlighted darker
    ["#62a0ea"], # 6. highlighted dark
    ["#8ff0a4"], # 7. urgent workspace
    ["#f66151"], # 8. urgent client
    ["#dc8add"], # 9. layout
]
catppuccin = [
    ["#1E1E2E"], # 0. panel background
    ["#313244"], # 1. darker / lighter background
    ["#D9E0EE"], # 2. active / default widget foreground
    ["#988BA2"], # 3. inactive window border / foreground
    ["#F9E2AF"], # 4. highlight color
    ["#89B4FA"], # 5. highlighted darker
    ["#89B4FA"], # 6. highlighted dark
    ["#94E2D5"], # 7. urgent workspace
    ["#F38BA8"], # 8. urgent client
    ["#CBA6F7"], # 9. layout
]
dracula = [
    ["#282a36"], # 0. panel background
    ["#383A59"], # 1. darker / lighter background
    ["#f8f8f2"], # 2. active / default widget foreground
    ["#6272a4"], # 3. inactive window border / foreground
    ["#f1fa8c"], # 4. highlight color
    ["#d6acff"], # 5. highlighted darker
    ["#bd93f9"], # 6. highlighted dark
    ["#50fa7b"], # 7. urgent workspace
    ["#ff5555"], # 8. urgent client
    ["#ff92df"], # 9. layout
]
duskfox = [
    ["#232136"], # 0. panel background
    ["#393552"], # 1. darker / lighter background
    ["#E0DEF4"], # 2. active / default widget foreground
    ["#47407D"], # 3. inactive window border / foreground
    ["#F9CB8C"], # 4. highlight color
    ["#65B1CD"], # 5. highlighted darker
    ["#569FBA"], # 6. highlighted dark
    ["#9CCFD8"], # 7. urgent workspace
    ["#EB6F92"], # 8. urgent client
    ["#C4A7E7"], # 9. layout
]
everforest_dark = [
    ["#2B3339"], # 0. panel background
    ["#4A555B"], # 1. darker / lighter background
    ["#D3C6AA"], # 2. active / default widget foreground
    ["#4A555B"], # 3. inactive window border / foreground
    ["#DBBC7F"], # 4. highlight color
    ["#7FBBB3"], # 5. highlighted darker
    ["#7FBBB3"], # 6. highlighted dark
    ["#83C092"], # 7. urgent workspace
    ["#E67E80"], # 8. urgent client
    ["#D699B6"], # 9. layout
]
gruvbox_dark = [
    ["#282828"], # 0. panel background
    ["#1D2021"], # 1. darker / lighter background
    ["#EBDBB2"], # 2. active / default widget foreground
    ["#928374"], # 3. inactive window border / foreground
    ["#FABD2F"], # 4. highlight color
    ["#83A598"], # 5. highlighted darker
    ["#458588"], # 6. highlighted dark
    ["#8EC07C"], # 7. urgent workspace
    ["#FB4934"], # 8. urgent client
    ["#D3869B"], # 9. layout
]
gruvbox_dark_material = [
    ["#282828"], # 0. panel background
    ["#32302F"], # 1. darker / lighter background
    ["#D4BE98"], # 2. active / default widget foreground
    ["#5A524C"], # 3. inactive window border / foreground
    ["#D8A657"], # 4. highlight color
    ["#7DAEA3"], # 5. highlighted darker
    ["#A9B665"], # 6. highlighted dark
    ["#89B482"], # 7. urgent workspace
    ["#EA6962"], # 8. urgent client
    ["#D3869B"], # 9. layout
]
gruvbox_dark_material_mix = [
    ["#282828"], # 0. panel background
    ["#32302F"], # 1. darker / lighter background
    ["#E2CCA9"], # 2. active / default widget foreground
    ["#5A524C"], # 3. inactive window border / foreground
    ["#E9B143"], # 4. highlight color
    ["#80AA9E"], # 5. highlighted darker
    ["#A9B665"], # 6. highlighted dark
    ["#8BBA7F"], # 7. urgent workspace
    ["#F2594B"], # 8. urgent client
    ["#D3869B"], # 9. layout
]
monokai = [
    ["#2D2A2E"], # 0. panel background
    ["#403E41"], # 1. darker / lighter background
    ["#FCFCFA"], # 2. active / default widget foreground
    ["#727072"], # 3. inactive window border / foreground
    ["#FFD866"], # 4. highlight color
    ["#FC9867"], # 5. highlighted darker
    ["#AB9DF2"], # 6. highlighted dark
    ["#A9DC76"], # 7. urgent workspace
    ["#FF6188"], # 8. urgent client
    ["#AB9DF2"], # 9. layout
]
monokai_pro = [
    ["#2D2A2E"], # 0. panel background
    ["#403E41"], # 1. darker / lighter background
    ["#FCFCFA"], # 2. active / default widget foreground
    ["#727072"], # 3. inactive window border / foreground
    ["#FFD866"], # 4. highlight color
    ["#FC9867"], # 5. highlighted darker
    ["#AB9DF2"], # 6. highlighted dark
    ["#A9DC76"], # 7. urgent workspace
    ["#FF6188"], # 8. urgent client
    ["#AB9DF2"], # 9. layout
]
nightfall = [
    ["#161622"], # 0. panel background
    ["#2B2B40"], # 1. darker / lighter background
    ["#D1D1E0"], # 2. active / default widget foreground
    ["#5B5B9A"], # 3. inactive window border / foreground
    ["#E8C47D"], # 4. highlight color
    ["#93B1EC"], # 5. highlighted darker
    ["#7DA1E8"], # 6. highlighted dark
    ["#88C0D0"], # 7. urgent workspace
    ["#E87D8F"], # 8. urgent client
    ["#B37DE8"], # 9. layout
]
nord = [
    ["#2E3440"], # 0. panel background
    ["#3B4252"], # 1. darker / lighter background
    ["#D8DEE9"], # 2. active / default widget foreground
    ["#4C566A"], # 3. inactive window border / foreground
    ["#EBCB8B"], # 4. highlight color
    ["#81A1C1"], # 5. highlighted darker
    ["#5E81AC"], # 6. highlighted dark
    ["#88C0D0"], # 7. urgent workspace
    ["#BF616A"], # 8. urgent client
    ["#B48EAD"], # 9. layout
]
onedark = [
    ["#282C34"], # 0. panel background
    ["#3E4452"], # 1. darker / lighter background
    ["#ABB2BF"], # 2. active / default widget foreground
    ["#5C6370"], # 3. inactive window border / foreground
    ["#E5C07B"], # 4. highlight color
    ["#61AFEF"], # 5. highlighted darker
    ["#61AFEF"], # 6. highlighted dark
    ["#56B6C2"], # 7. urgent workspace
    ["#E06C75"], # 8. urgent client
    ["#C678DD"], # 9. layout
]
solarized_dark = [
    ["#002B36"], # 0. panel background
    ["#073642"], # 1. darker / lighter background
    ["#93A1A1"], # 2. active / default widget foreground
    ["#657B83"], # 3. inactive window border / foreground
    ["#B58900"], # 4. highlight color
    ["#268BD2"], # 5. highlighted darker
    ["#6C71C4"], # 6. highlighted dark
    ["#2AA198"], # 7. urgent workspace
    ["#DC322F"], # 8. urgent client
    ["#D33682"], # 9. layout
]
sonokai = [
    ["#2D2A2E"], # 0. panel background
    ["#1A181A"], # 1. darker / lighter background
    ["#E3E1E4"], # 2. active / default widget foreground
    ["#848089"], # 3. inactive window border / foreground
    ["#E5C463"], # 4. highlight color
    ["#7ACCD7"], # 5. highlighted darker
    ["#AB9DF2"], # 6. highlighted dark
    ["#9ECD6F"], # 7. urgent workspace
    ["#F85E84"], # 8. urgent client
    ["#AB9DF2"], # 9. layout
]
tokyo_night = [
    ["#1A1B26"], # 0. panel background
    ["#24283B"], # 1. darker / lighter background
    ["#C0CAF5"], # 2. active / default widget foreground
    ["#414868"], # 3. inactive window border / foreground
    ["#E0AF68"], # 4. highlight color
    ["#7AA2F7"], # 5. highlighted darker
    ["#7AA2F7"], # 6. highlighted dark
    ["#7DCFFF"], # 7. urgent workspace
    ["#F7768E"], # 8. urgent client
    ["#BB9AF7"], # 9. layout
]
void = [
    ["#17141F"], # 0. panel background
    ["#221E2F"], # 1. darker / lighter background
    ["#D4D1E0"], # 2. active / default widget foreground
    ["#5B517B"], # 3. inactive window border / foreground
    ["#DDC788"], # 4. highlight color
    ["#9D88DD"], # 5. highlighted darker
    ["#8D75D7"], # 6. highlighted dark
    ["#75D7BE"], # 7. urgent workspace
    ["#D78D75"], # 8. urgent client
    ["#C788DD"], # 9. layout
]
colors = nord

def def_layout_theme():
    return {
        # Colors:
        "active_bg": colors[4],
        "active_fg": colors[0],
        "bg_color": colors[0],
        "border_focus": colors[4],
        "border_focus_stack": colors[8],
        "border_normal": colors[3],
        "border_normal_stack": colors[6],
        "border_urgent": colors[8],
        "inactive_bg": colors[0],
        "inactive_fg": colors[2],
        "section_fg": colors[2],
        "urgent_bg": colors[8],
        "urgent_fg": colors[0],
        # Fonts:
        "font": 'Caskaydia Cove Nerd Font',
        "fontsize": 16,
        "section_fontsize": 14,
        # Size parameters
        "border_width": 3,
        "margin": 0,
        "panel_width": 175,
        "single_margin": 0,
        "single_border_width": 0,
        # Miscellaneous layout options
        "sections": [
            "Main", "Background"
        ],
    }

### Layout themes ###
layout_theme = def_layout_theme()

layouts = [
    layout.Columns(
        **layout_theme,
        grow_amount = 5,
    ),
    # layout.MonadTall(
    #     **layout_theme,
    #     new_client_position = 'after_current',
    #     # new_client_position = 'top',
    #     ),
    # layout.MonadWide(
    #     **layout_theme,
    #     new_client_position = 'after_current',
    #     ),
    # layout.Bsp(**layout_theme),
    layout.TreeTab(**layout_theme),
    # layout.Max(**layout_theme),
]

widget_defaults = dict(
    font = 'FiraCode Nerd Font',
    # font = 'FiraCode Nerd Font',
    fontsize = 13,
    padding = 3,
    foreground = colors[2],
)
extension_defaults = widget_defaults.copy()

screens = [
    Screen(
        top = bar.Bar(
            [
                widget.GroupBox(
                    active = colors[2],
                    inactive = colors[3],
                    highlight_color = colors[1],
                    urgent = colors[7],
                    this_current_screen_border = colors[4],
                    block_highlight_text_color = colors[4],
                    rounded = "true",
                    highlight_method = "line",
                    disable_drag = True,
                    hide_unused = True
                    ),
                widget.Sep(
                    padding = 3,
                    foreground = colors[0]
                    ),
                widget.Prompt(
                    fmt = "> {}",
                    foreground = colors[7]
                ),
                # widget.TextBox(
                    # text = '',
                    # background = colors[1],
                    # foreground = colors[0],
                    # padding = 0,
                    # fontsize = 17,
                    # ),
                widget.CurrentLayout(
                    # background = colors[1],
                    foreground = colors[9],
                    font = "FiraCode Nerd Font Bold"
                    ),
                widget.Sep(
                    padding = 3,
                    foreground = colors[0]
                    ),
                widget.WindowName(
                    # font = "Caskaydia Cove Nerd Font Bold",
                    foreground = colors[5]
                ),
                widget.Sep(
                    padding = 1,
                    foreground = colors[0]
                    ),
                widget.TextBox(
                    text = '',
                    # background = colors[],
                    foreground = colors[1],
                    padding = 0,
                    fontsize = 17,
                    ),
                widget.TextBox(
                    text = " ",
                    fontsize = 14,
                    background = colors[1],
                    ),
                widget.KeyboardLayout(
                    update_interval = 1,
                    configured_keyboards = ['gb', 'ua'],
                    background = colors[1],
                    ),
                widget.TextBox(
                    text = '',
                    background = colors[1],
                    foreground = colors[0],
                    padding = 0,
                    fontsize = 17,
                    ),
                widget.TextBox(
                    text = "墳",
                    fontsize = 14,
                    background = colors[0],
                    ),
                widget.Volume(),
                widget.TextBox(
                    text = '',
                    background = colors[0],
                    foreground = colors[1],
                    padding = 0,
                    fontsize = 17,
                    ),
                widget.Battery(
                    update_interval = 1,
                    background = colors[1],
                    full_foreground = colors[7],
                    low_foreground = colors[8],
                    charge_char = ' Charging',
                    discharge_char = ' Discharging',
                    full_char = ' Fully charged',
                    empty_char = ' BATTERY LOW',
                    unknown_char = '',
                    format = '{char} {percent:2.0%} : {hour:d}:{min:02d} left'
                    ),
                widget.TextBox(
                    text = '',
                    background = colors[1],
                    foreground = colors[0],
                    padding = 0,
                    fontsize = 17,
                    ),
                widget.TextBox(
                    text = " ",
                    fontsize = 14,
                    mouse_callbacks={'Button1': lambda: qtile.cmd_spawn("/home/vlad/.suckless/dmenu-flexipatch/scripts/networkmanager_dmenu")}
                    ),
                widget.Wlan(
                    interface = 'wlp2s0',
                    mouse_callbacks={'Button1': lambda: qtile.cmd_spawn("/home/vlad/.suckless/dmenu-flexipatch/scripts/networkmanager_dmenu")}
                    ),
                widget.TextBox(
                    text = '',
                    foreground = colors[1],
                    padding = 0,
                    fontsize = 17,
                    ),
                widget.TextBox(
                    text = " ",
                    fontsize = 14,
                    background = colors[1],
                    ),
                widget.Clock(
                    format = '%d.%m.%Y',
                    background = colors[1],
                    ),
                widget.TextBox(
                    text = '',
                    background = colors[1],
                    foreground = colors[0],
                    padding = 0,
                    fontsize = 17,
                    ),
                widget.TextBox(
                    text = " ",
                    fontsize = 14,
                    background = colors[0],
                    ),
                widget.Clock(
                    # format = '%b %d (%a) %T',
                    format = '%T',
                    # format = '%d.%m.%Y %T',
                    background = colors[0],
                    ),
                widget.Systray(
                    background = colors[0],
                    ),
                widget.Sep(
                    padding = 3,
                    foreground = colors[0],
                    background = colors[0]
                    ),
            ],
            20,
            background = colors[0],
        ),
    ),
]

# Drag floating layouts.
mouse = [
    Drag([mod], "Button1", lazy.window.set_position_floating(),
         start = lazy.window.get_position()),
    Drag([mod], "Button3", lazy.window.set_size_floating(),
         start = lazy.window.get_size()),
    Click([mod], "Button2", lazy.window.bring_to_front())
]

dgroups_key_binder = None
dgroups_app_rules = []  # type: List
follow_mouse_focus = True
bring_front_click = False
cursor_warp = False
floating_layout = layout.Floating(float_rules=[
    # Run the utility of `xprop` to see the wm class and name of an X client.
    *layout.Floating.default_float_rules,
    Match(wm_class = 'confirmreset'),  # gitk
    Match(wm_class = 'makebranch'),  # gitk
    Match(wm_class = 'maketag'),  # gitk
    Match(wm_class = 'ssh-askpass'),  # ssh-askpass
    Match(title = 'branchdialog'),  # gitk
    Match(title = 'pinentry'),  # GPG key password entry
], **layout_theme)
auto_fullscreen = True
focus_on_window_activation = "smart"
reconfigure_screens = True

# If things like steam games want to auto-minimize themselves when losing
# focus, should we respect this or not?
auto_minimize = True

wmname = "LG3D"
