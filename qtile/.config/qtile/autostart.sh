#!/bin/bash
lxpolkit &
pipewire &
dunst &
sxhkd -c ~/.config/qtile/layoutKeyChords &
# killall volumeicon
# nm-applet &
# blueberry-tray &
# blueman-tray &
# killall redshift-gtk
# redshift-gtk &
nitrogen --restore &
# feh --bg-scale --randomize ~/Изображения/Wallpapers/*
# volumeicon &
# xfce4-clipman &
# mate-power-manager &
# mate-volume-control-status-icon &
picom &
# picom --config ~/.config/qtile/picom.conf &
# setxkbmap "us,ua" ",winkeys" "grp:alt_shift_toggle" &
# setxkbmap "us,ru" ",winkeys" "grp:alt_shift_toggle" &
