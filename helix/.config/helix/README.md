# Helix text editor config
This if my configuration of Helix text editor.

I use several language servers for autocompletion: 
[bash-language-server](https://github.com/bash-lsp/bash-language-server),
[clangd](https://clangd.llvm.org/),
[gopls](https://pkg.go.dev/golang.org/x/tools/gopls),
[lua-language-server](https://github.com/sumneko/lua-language-server),
[pylsp](https://github.com/python-lsp/python-lsp-server),
[rust-analyzer](https://github.com/rust-lang/rust-analyzer) and
[taplo](https://github.com/tamasfe/taplo).

![screenshot](https://gitlab.com/nnonfree/dotfiles/-/raw/main/helix/.config/helix/screenshot.png)
