"  _       _ _         _           
" (_)_ __ (_) |___   _(_)_ __ ___  
" | | '_ \| | __\ \ / / | '_ ` _ \ 
" | | | | | | |_ \ V /| | | | | | |
" |_|_| |_|_|\__(_)_/ |_|_| |_| |_|
"						by pantheonfordevil

" Setting variables
set number
set relativenumber
set autoindent
set ic
set tabstop=4
set shiftwidth=4
set smarttab
set softtabstop=4
set mouse=a
set encoding=UTF-8

" Plugins
call plug#begin()
" Color schemes
Plug 'navarasu/onedark.nvim'
Plug 'romgrk/doom-one.vim'
Plug 'rakr/vim-one'
Plug 'Mofiqul/dracula.nvim'
Plug 'morhetz/gruvbox'
Plug 'gosukiwi/vim-atom-dark'
Plug 'rafi/awesome-vim-colorschemes'
Plug 'phanviet/vim-monokai-pro'
Plug 'folke/tokyonight.nvim', { 'branch': 'main' }
Plug 'shaunsingh/solarized.nvim'
Plug 'lifepillar/vim-solarized8'
Plug 'catppuccin/nvim', {'as': 'catppuccin'}
Plug 'mhartington/oceanic-next'
Plug 'tomasiser/vim-code-dark'
Plug 'chriskempson/vim-tomorrow-theme'
Plug 'drewtempelmeyer/palenight.vim'
Plug 'ishan9299/nvim-solarized-lua'
Plug 'sainnhe/edge'
Plug 'sainnhe/sonokai'
Plug 'sainnhe/everforest'
Plug 'sainnhe/gruvbox-material'
Plug 'metalelf0/base16-black-metal-scheme'
Plug 'EdenEast/nightfox.nvim'
Plug 'ayu-theme/ayu-colors'
Plug 'nekonako/xresources-nvim'
Plug 'dracula/vim', { 'as': 'dracula' }
Plug 'tjdevries/colorbuddy.nvim'
Plug 'bbenzikry/snazzybuddy.nvim'
Plug 'marciomazza/vim-brogrammer-theme'

" Coc.nvim - autocompletion
Plug 'neoclide/coc.nvim', {'branch': 'release'}

" Other
Plug 'folke/which-key.nvim'
Plug 'scrooloose/nerdtree'
Plug 'vifm/vifm.vim'
Plug 'majutsushi/tagbar'
" Plug 'vim-airline/vim-airline'
" Plug 'vim-airline/vim-airline-themes'
Plug 'tpope/vim-surround'
Plug 'tpope/vim-commentary'
Plug 'ap/vim-css-color'
Plug 'ryanoasis/vim-devicons'
Plug 'terryma/vim-multiple-cursors'
" Plug 'neoclide/coc.nvim'
Plug 'ryanoasis/vim-devicons'
Plug 'tribela/vim-transparent'
Plug 'xolox/vim-notes'
Plug 'xolox/vim-misc'
Plug 'nvim-lualine/lualine.nvim'
" Plug 'itchyny/lightline.vim'
Plug 'kyazdani42/nvim-web-devicons'
Plug 'startup-nvim/startup.nvim'
Plug 'nvim-telescope/telescope-file-browser.nvim'
Plug 'nvim-telescope/telescope.nvim'
Plug 'nvim-lua/plenary.nvim'
Plug 'vim-scripts/TeTrIs.vim'
Plug 'sheerun/vim-polyglot'
Plug 'nvim-treesitter/nvim-treesitter'
Plug 'delphinus/auto-cursorline.nvim'
Plug 'cloudhead/neovim-fuzzy'
Plug 'rust-lang/rust.vim'
call plug#end()

" Recompile suckless software automatically:
" autocmd BufWritePost config.h,config.def.h !sudo make install -S
" Run xrdb whenerver Xdefaults or Xresources are updated:
autocmd BufWritePost ~/.Xresources,~/.Xdefaults !xrdb %

set inccommand=nosplit
set completeopt-=preview
set clipboard+=unnamedplus

" Neovide options
if exists("g:neovide")
    set guifont=JetBrainsMono\ Nerd\ Font:h11
    let g:neovide_transparency = 1
endif

" Colorscheme options
let g:doom_one_terminal_colors = v:true 
" let g:edge_style = 'neon'
let g:sonokai_style = 'shusia'
let g:everforest_background = 'hard'
let g:gruvbox_material_foreground = 'mix'
let g:tokyonight_style = "night"
let g:oceanic_next_terminal_bold = 1
let g:oceanic_next_terminal_italic = 1

" Syntax options
syntax on
if (empty($TMUX))
  if (has("nvim"))
    let $NVIM_TUI_ENABLE_TRUE_COLOR=1
  endif
  if (has("termguicolors"))
    set termguicolors
  endif
endif
let g:one_allow_italics = 1
colorscheme nord

" Lightline
" let g:lightline = {
"       \ 'colorscheme': 'everforest',
"       \ }

" Lua-configured plugins
lua << End
require('lualine').setup{
	options = {
      theme = "auto",
      component_separators = { left = ' ', right = ''},
      section_separators = { left = ' ', right = ''},
	}
}
require("which-key").setup{}
require("startup").setup{theme = "my_theme"}
require("auto-cursorline").setup{}
require("telescope").load_extension "file_browser"
End

" Remapping some keybinds
cnoreabbrev W w
cnoreabbrev Ĝ w
cnoreabbrev Q q
cnoreabbrev Ŝ q
cnoreabbrev X x
cnoreabbrev Ĉ x
cnoreabbrev ; :
cnoreabbrev o e
cnoreabbrev open edit
cnoreabbrev hsplit split h
" cnoreabbrev theme colorscheme
inoremap " ""<left>
inoremap ' ''<left>
inoremap ( ()<left>
inoremap [ []<left>
inoremap { {}<left>
" inoremap {<CR> {<CR>}<ESC>O
" inoremap {;<CR> {<CR>};<ESC>O
let mapleader = "\<Space>"

" Adding some keybinds
" nnoremap <C-w><C-h> :split h<CR>

nnoremap <A-c> :Commentary<CR> 
nnoremap <C-t> :TagbarToggle<CR>
nnoremap <C-\> :NERDTreeToggle<CR>
nnoremap <C-c><C-l> :set background=light<CR> 
nnoremap <C-c><C-d> :set background=dark<CR> 
" nnoremap <C-c><C-l> :colorscheme dayfall<CR>
" nnoremap <C-c><C-d> :colorscheme nightfall<CR>
nnoremap <C-c><C-t> :TransparentToggle<CR>

nnoremap <leader>qq <cmd> :q  <cr>
nnoremap <leader>qQ <cmd> :q! <cr>
nnoremap <leader>qw <cmd> :wq <cr>
nnoremap <leader>qW <cmd> :wq!<cr>

nnoremap <leader>ŝŝ <cmd> :q  <cr>
nnoremap <leader>ŝŜ <cmd> :q! <cr>
nnoremap <leader>ŝĝ <cmd> :wq <cr>
nnoremap <leader>ŝĜ <cmd> :wq!<cr>

nnoremap <leader>fs <cmd> :w  <cr>

" Find files using Telescope command-line sugar.
nnoremap <leader>ff <cmd>Telescope find_files<cr>
nnoremap <leader>fr <cmd>Telescope oldfiles<cr>
nnoremap <leader>fg <cmd>Telescope live_grep<cr>
nnoremap <leader>fh <cmd>Telescope help_tags<cr>
nnoremap <leader>ht <cmd>Telescope colorscheme<cr>
nnoremap <leader>.  <cmd>Telescope file_browser<cr>
nnoremap <leader>,  <cmd>Telescope buffers<cr>

nnoremap <leader>hrc <cmd> :source ~/.config/nvim/init.vim  <CR>
nnoremap <leader>hrp <cmd> :PlugInstall <CR> | :PlugUpdate  <CR>

nnoremap <leader>oc <cmd> :e ~/.config/nvim/init.vim <CR>
nnoremap <leader>ot <cmd> :terminal                  <CR>

"coc.nvim :
inoremap <silent><expr> <CR> coc#pum#visible() ? coc#pum#confirm()
                              \: "\<C-g>u\<CR>\<c-r>=coc#on_enter()\<CR>"

" inoremap <silent><expr> <TAB>
      \ coc#pum#visible() ? coc#pum#next(1) :
      \ CheckBackspace() ? "\<Tab>" :
      \ coc#refresh()
" inoremap <expr><S-TAB> coc#pum#visible() ? coc#pum#prev(1) : "\<C-h>"
