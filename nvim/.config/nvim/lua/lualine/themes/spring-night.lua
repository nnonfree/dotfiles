local spring-night = {}

spring-night.normal = {
	a = { bg = cp.blue, fg = cp.mantle, gui = "bold" },
	b = { bg = cp.surface1, fg = cp.blue },
	c = { bg = cp.mantle, fg = cp.text },
}

spring-night.insert = {
	a = { bg = , fg = cp.base, gui = "bold" },
	b = { bg = cp.surface1, fg = cp.teal },
}

spring-night.command = {
	a = { bg = cp.peach, fg = cp.base, gui = "bold" },
	b = { bg = cp.surface1, fg = cp.peach },
}

spring-night.visual = {
	a = { bg = cp.mauve, fg = cp.base, gui = "bold" },
	b = { bg = cp.surface1, fg = cp.mauve },
}

spring-night.replace = {
	a = { bg = cp.red, fg = cp.base, gui = "bold" },
	b = { bg = cp.surface1, fg = cp.red },
}

spring-night.inactive = {
	a = { bg = '#282843', fg = cp.blue },
	b = { bg = '#282843', fg = cp.surface1, gui = "bold" },
	c = { bg = '#282843', fg = cp.overlay0 },
}

return spring-night
