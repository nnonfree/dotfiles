# Dotfiles
This is my dotfiles repo.

To set them up easily, just run `stow */` in the root of this repository.

I use Fedora, so you can take a look at my post-installation documentation [here](https://gitlab.com/nnonfree/dotfiles/-/blob/main/Fedora%20install%20documentation.md).

![screenshot](https://gitlab.com/nnonfree/dotfiles/-/raw/main/screenshot2.png)
