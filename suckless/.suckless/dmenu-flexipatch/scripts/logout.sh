#!/bin/sh

# Load dmenu config
# shellcheck source=/dev/null
[ -f "$HOME/.dmenurc" ] && . "$HOME/.dmenurc" || DMENU='dmenu'

# Menu items
items="lock
suspend
reboot
poweroff"

# Open menu
selection=$(printf '%s' "$items" | $DMENU -p "Power:")

case $selection in
	lock)
		slock
		# i3lock -c 282C34
		;;
	suspend)
		loginctl suspend || systemctl suspend
		;;
	reboot)
		loginctl reboot || systemctl reboot
		;;
	halt|poweroff|shutdown)
		loginctl poweroff || systemctl poweroff
		;;
esac

exit
