[ -f "$HOME/.dmenurc" ] && . "$HOME/.dmenurc" || DMENU='dmenu'

items="mount
unmount"

selection=$(printf '%s' "$items" | $DMENU -p "Option:")

case $selection in
	mount)
		/home/vlad/.suckless/dmenu-flexipatch/scripts/mount.sh
		;;
	unmount)
		/home/vlad/.suckless/dmenu-flexipatch/scripts/unmount.sh
		;;
esac

exit
